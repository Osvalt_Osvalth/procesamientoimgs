# Importing the libraries
import numpy as np
import cv2
from matplotlib import pyplot as plt
from sklearn.cluster import KMeans

from NDVI_files import rutas_D_Archivos as myPath

def getclustering(ImG_Original, kmeans):
  K_val = kmeans
  I = cv2.imread( ImG_Original )#Leemos los valores que recibimos
  I1 = cv2.cvtColor(I,cv2.COLOR_BGR2RGB)
  a = I1.reshape((-1,3))
  a1 = np.float32(a)

  # wcss = []
  # for i in range(1,6):
  #   k_means = KMeans(n_clusters=i, init="k-means++", max_iter=300, n_init=10, random_state=0)
  #   k_means.fit(a1)
  #   wcss.append(k_means.inertia_)
  #
  # plt.plot(range(1,6), wcss)
  # #plt.show()

  nameHistoCluster = myPath.getPathByCategory('HISTOGRAMA_CLUSTER', histo=None)

  colors = ["green", "red", "yellow", "orange", "dodgerblue", "deeppink"]
  labelCluster = ["Cluster 1", "Cluster 2", "Cluster 3", "Cluster 4", "Cluster 5", "Cluster 6"]

  kmeans = KMeans(n_clusters=K_val, init="k-means++", max_iter=300, n_init=10, random_state=0)
  y_kmeans = kmeans.fit_predict(a1)
  for n in range(0,K_val):
    plt.scatter(a1[y_kmeans == n,0],a1[y_kmeans == n,1], s=4, c=colors[n], label=labelCluster[n])

  plt.scatter(kmeans.cluster_centers_[:,0],kmeans.cluster_centers_[:,1], s=10, c="blue", label="Baricentros")
  plt.title("Cluster del NDVI")
  plt.xlabel("Rango de la escala en grises")
  plt.ylabel("Muestras por aparoión de colores")
  plt.legend()
  plt.savefig(nameHistoCluster, dpi=256)  # 2600
  plt.show()
  plt.close('all')
  return nameHistoCluster

