# -*- coding: utf-8 -*-
"""
Created on Mon Jun  1 15:47:57 2020

@author: music
"""
import os
from pathlib import Path
import time   #Para obtener la hora para el guardado de la imagen

'''-------------------------------------------------------Ruta del Programa '''
PATH = str(os.getcwd()).replace('\\', '/')
'''--------------------Ruta general de las carpetas con imagenes Resultantes'''
PATH_SAVE_GENERAL = PATH + '/RESULTADOS/RESULTADOS_'
###############################################################################
def crearDirectorio(newDirectorio):
    try:
        Path(newDirectorio).mkdir( parents=True, exist_ok=True )
    except FileExistsError:
        print("Error en el archivo o diectorio")
    else:
        print("El directorio fue creado")

'''Función para crear la ruta de imagen a guardar en diferentes Directorio con 
hora y fecha   '''
def get_time_ms():
    '''   Regresamos el tiempo en milisegundos  '''
    return int(round(time.time() * 1000))
'''-------------------------GENERAR PATH POR CATEGORIA DEL DIRECTORIO A USAR'''
def createPathSaveCategory(dirByCategory, nomCategory):
    return dirByCategory + '/IMG_' + str(get_time_ms()) +'_'+ nomCategory + '.TIF'

def getPathByCategory( NameCategory, histo = None):
    # print('hhhhhhhhh---->' + histo)
    if histo is None:
        pathreceived = PATH_SAVE_GENERAL + NameCategory
        crearDirectorio( str( pathreceived ) )
        PathByCategory = createPathSaveCategory( pathreceived  , NameCategory )
        return PathByCategory
    else:
        pathreceived = PATH_SAVE_GENERAL + NameCategory
        pathreceivedHISTO = PATH_SAVE_GENERAL + NameCategory + '/'+ histo
        
        crearDirectorio( str( pathreceived ) )
        crearDirectorio( str( pathreceivedHISTO ) )
        
        PathByCategory = createPathSaveCategory( pathreceived  , NameCategory )
        PathByCategoryH = createPathSaveCategory( pathreceivedHISTO  , histo )
        
        return PathByCategory, PathByCategoryH







