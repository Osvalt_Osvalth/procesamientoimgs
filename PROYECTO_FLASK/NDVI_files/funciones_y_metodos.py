# -*- coding: utf-8 -*-
"""
Created on Mon Jun  1 15:30:44 2020

@author: Osvalth

███╗░░██╗██████╗░██╗░░░██╗██╗
████╗░██║██╔══██╗██║░░░██║██║
██╔██╗██║██║░░██║╚██╗░██╔╝██║
██║╚████║██║░░██║░╚████╔╝░██║
██║░╚███║██████╔╝░░╚██╔╝░░██║
╚═╝░░╚══╝╚═════╝░░░░╚═╝░░░╚═╝
"""

##########################################################################     NDVI

'''--------------------------------------------------------Librerias a usar '''
import numpy as np
from matplotlib import pyplot as plt
import cv2

'''----------------------------------------------------Mis Librerias a usar '''
from NDVI_files import color_Barra as col_Bar
from NDVI_files import rutas_D_Archivos as myPath
from HISTOGRAMA import graphPie as graphPie1
'''---------------------------------------Mis Definiciones o métodos a usar '''
def get_ndvi(red, nir):
    '''Compute normalized difference vegetation index.'''
    np.seterr(divide='ignore', invalid='ignore')  # Permitimos division entre cero
    #ndvi = np.divide((nir - red), (nir + red))
    ndvi = (nir - red) / (nir + red)  # The NDVI formula
    ndvi = 1.32-ndvi#Valor de corrección 1.32
    return ndvi

def apply_custom_colormap(image_gray, cmap=col_Bar.cmapColores_1):
    '''Apply a custom colormap to a grayscale image and output the
    result as a BGR image.

    Credit: https://stackoverflow.com/a/52498778
    '''
    assert image_gray.dtype == np.uint8, 'must be np.uint8 image'
    if image_gray.ndim == 3:
        image_gray = image_gray.squeeze(-1)#-1

    ''' inicializar el mapa de colores '''
    sm = plt.cm.ScalarMappable(cmap=cmap)

    '''   Obtenemos un rango de colores   '''
    color_range = sm.to_rgba(np.linspace(0, 1, 256))[:, 0:3]  # color range RGBA => RGB
    color_range = (color_range * 255).astype(np.uint8)  # [0,1] => [0,255]

    #color_range = np.squeeze( np.dstack([color_range[:, 2], color_range[:, 1], color_range[:, 0]]),0)  # RGB => BGR

    # Apply colormap for each channel individually
    channels = [cv2.LUT(image_gray, color_range[:, i]) for i in range(3)]
    return np.dstack(channels)

'''      FUNCIÓN QUE GUARDA IMAGEN USANDO LA HORA Y FECHA COMO NOMBRE   '''
def save_image( ndvi_colorized1 , rut):
    '''    Directorio de guardado    '''
    directorio = rut
    '''    Para quitar los valores de los ejes     '''
    plt.gca().axes.get_yaxis().set_visible(False)
    plt.gca().axes.get_xaxis().set_visible(False)
    '''    Mostramos la imagen con la paleta de colores   '''
    plt.imshow(ndvi_colorized1, cmap = col_Bar.cmapColores_BAR)
    plt.colorbar()
    plt.clim(vmax=0 , vmin=1)
    plt.savefig(directorio, bbox_inches='tight', pad_inches=0, dpi=256)#2600
    plt.close('all')
    return directorio

def save_imageSingle( ndvi_colorized1 , rut):
    '''    Directorio de guardado    '''
    directorio = rut
    '''    Para quitar los valores de los ejes     '''
    plt.gca().axes.get_yaxis().set_visible(False)
    plt.gca().axes.get_xaxis().set_visible(False)
    plt.figure(frameon=False)
    plt.axis('off')
    plt.imshow(ndvi_colorized1)
    plt.savefig(directorio, bbox_inches='tight', pad_inches=0, dpi=256)#2600
    plt.close(1)
    return directorio

def getGraphPie(verde, amarillo, rojo, negro):
    tipos = [verde, amarillo, rojo, negro]
    nombres = ["Verde", "Amarillo", "Rojo", "Negro"]
    colores = ["#00FF00", "#FFFF00", "#FF0000", "#0023A0"]
    pathImg = myPath.getPathByCategory('GRAPH_PIE', histo=None)
    plt.pie(tipos, labels=nombres, autopct="%0.1f %%", colors=colores)
    plt.axis("equal")
    plt.savefig(pathImg, dpi=256)  # 2600
    plt.show()
    plt.close('all')
    return pathImg

def color2GYR(ndvi):
    img2 = ndvi
    rows, cols = ndvi.shape
    dist = np.zeros((rows, cols, 3), dtype=ndvi.dtype)
    verde = 0
    amarillo = 0
    rojo = 0
    negro = 0
    for i in range(rows):
        for j in range(cols):
            if (img2[i, j] >= 0.66):
                dist[i, j] = [0, 255, 0]
                verde+=1
            if (img2[i, j] >= 0.33 and img2[i, j] < 0.66):
                dist[i, j] = [255, 255, 0]
                amarillo+=1
            if (img2[i, j] > 0 and img2[i, j] < 0.33):
                dist[i, j] = [255, 0, 0]
                rojo+=1
            if (img2[i, j] <= 0):
                dist[i, j] = [0, 0, 0]
                negro+=1
            else:
                1
    plt.close('all')
    return dist, verde, amarillo, rojo, negro

def color2GYR_single(ndvi):
    img2 = ndvi
    rows, cols = ndvi.shape
    dist = np.zeros((rows, cols, 3), dtype=ndvi.dtype)
    for i in range(rows):
        for j in range(cols):
            if (img2[i, j] >= 0.66):
                dist[i, j] = [0, 255, 0]
            if (img2[i, j] >= 0.33 and img2[i, j] < 0.66):
                dist[i, j] = [255, 255, 0]
            if (img2[i, j] > 0 and img2[i, j] < 0.33):
                dist[i, j] = [255, 0, 0]
            if (img2[i, j] <= 0):
                dist[i, j] = [0, 0, 0]
            else:
                1
    plt.close ('all')
    return dist

def imageProcessing (NDVI):
    '''    Coloreamos la imagen   '''
    ndvi_colorized, verde, amarillo, rojo, negro = color2GYR( NDVI )
    rutagraficaPastel = graphPie1.getGraphPie(verde, amarillo, rojo, negro)
    ndvi_colorized2 = color2GYR_single( NDVI )
    '''    Se genera la ruta ára el NDVI con barra y sin barra   '''
    guardarRutaCategoria = myPath.getPathByCategory('NDVI') # Se genera la ruta ára el NDVI con barra
    guardarRutaCategoriaSingle = myPath.getPathByCategory('NDVI_SINGLE')# Se genera la ruta ára el NDVI single
    '''    Guardamos imágen NDVI con paleta y sin paleta  '''
    imgBar = save_image(ndvi_colorized, guardarRutaCategoria)
    imgSingle = save_imageSingle(ndvi_colorized2, guardarRutaCategoriaSingle)
    '''    evolvemos direccion de las imágenes NDVI con paleta y NDVI single     '''
    return imgBar, imgSingle, rutagraficaPastel

