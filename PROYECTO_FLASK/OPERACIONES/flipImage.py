
import cv2

from NDVI_files import leerIMGs_TIF as saveReadIMAGE
from NDVI_files import rutas_D_Archivos as myPath

# Use Flip code: 0 = giro en el eje x, 1 = giro en el eje y, -1 = giro e ambos ejes.

def getFlipImage (ImagenA, valueCode):
    Imagen = cv2.imread(ImagenA , 0)
    resultado = cv2.flip(Imagen, valueCode)
    pathImg = myPath.getPathByCategory('IMAGE_FLIP', histo=None)
    saveReadIMAGE.writteImgBlack(pathImg, resultado)
    return pathImg