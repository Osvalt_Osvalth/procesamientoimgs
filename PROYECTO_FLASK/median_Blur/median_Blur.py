# -*- coding: utf-8 -*-
"""
Created on Sun Jun 14 19:10:32 2020

@author: music
"""
import cv2

from NDVI_files import leerIMGs_TIF as ReadWrittenIMGs
from NDVI_files import ndvi as ReadIMGs
from NDVI_files import rutas_D_Archivos as myPath
from HISTOGRAMA import histograma2 as hist

def getPathBynaryByName(nameImage , histo):
    guardarRutaCategoria = myPath.getPathByCategory(nameImage, histo)
    return guardarRutaCategoria

def calcularMedianBlur ( imagenSlider, Val1 ):
    img = ReadIMGs.obtenerImgNIR( imagenSlider )
    nameImg, nameHisto = getPathBynaryByName('MEDIAN_BLUR', histo = 'HISTOGRAMA')
    img = cv2.medianBlur( img , ksize = Val1 )
    ReadWrittenIMGs.writteImgBlack( nameImg, img )
    nameHisto2 = hist.getHistogram ( nameImg, nameHisto )
    return nameImg, nameHisto2

