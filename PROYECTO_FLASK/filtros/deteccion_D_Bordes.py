# -*- coding: utf-8 -*-
"""
Created on Wed Jul  1 00:36:32 2020

@author: music
"""
import cv2
import numpy as np
from scipy.ndimage import gaussian_filter
from NDVI_files import leerIMGs_TIF as ReadWrittenIMGs
from NDVI_files import rutas_D_Archivos as myPath

# scipy.ndimage.filters.gaussian_filter
# http://library.isr.ist.utl.pt/docs/scipy/generated/scipy.ndimage.filters.gaussian_filter.html
# https://handmap.github.io/gradients-and-edge-detection/
# =============================================================================
#           GENERAR NOMBRE DE IMAGEN ATRAVES DEL PATH
# =============================================================================
def getPathBynaryByName(nameImage , histo=None):
    guardarRutaCategoria = myPath.getPathByCategory(nameImage, histo)
    return guardarRutaCategoria
# =============================================================================
#FILTRO SOBEL (DERIVADA EN X, DERIVADA EN Y & MAGNITUD), CON/SIN FILTRO GAUSSIAN
# =============================================================================

def iniciarFiltroSobelbyOrder ( imagen, kernel, ordenGaussian ):
    global gray, nameImg_gaussian_filter
    if ordenGaussian == 0:
        gray = ReadWrittenIMGs.readIMGBORDER( imagen )
        print('sin')
    if ordenGaussian == 1 or ordenGaussian == 2:
        gray = ReadWrittenIMGs.readIMGBORDER( imagen )
        nameImg_gaussian_filter = getPathBynaryByName( 'GAUSSIAN_S' + str( ordenGaussian ) )
        gray = gaussian_filter( gray, sigma=3, order = ordenGaussian)
        ReadWrittenIMGs.writteImgBlack( nameImg_gaussian_filter, gray )# se guarda imagen
    
    '''---------------------- SOBEL EN X ----------------------'''
    nameImg_sobelX = getPathBynaryByName( 'XSOBEL' )
    sobel_X = cv2.Sobel( gray, cv2.CV_64F, 1, 0, ksize = kernel )      
        
    '''---------------------- SOBEL EN Y ----------------------'''
    nameImg_sobelY = getPathBynaryByName( 'YSOBEL' )
    sobel_Y = cv2.Sobel( gray, cv2.CV_64F, 0, 1, ksize = kernel )
    
    '''---------------------- MAGNITUDXY ----------------------'''
    derivate_X = np.uint8(np.absolute(sobel_X))
    ReadWrittenIMGs.writteImgBlack( nameImg_sobelX, derivate_X )# se guarda imagen
    
    derivate_Y = np.uint8(np.absolute(sobel_Y))
    ReadWrittenIMGs.writteImgBlack( nameImg_sobelY, derivate_Y )# se guarda imagen
    
    '''-----------------_ SUMA DE MAGNUTEDES ------------------'''
    nameImg_MAGNITUD = getPathBynaryByName( 'MAGNITUD' )
    MAGNITUD = cv2.bitwise_or(derivate_X, derivate_Y)
    ReadWrittenIMGs.writteImgBlack( nameImg_MAGNITUD, MAGNITUD )# se guarda imagen
    
    return nameImg_gaussian_filter, nameImg_sobelX, nameImg_sobelY, nameImg_MAGNITUD

# =============================================================================
#                           FILTRO LAPLACE
# =============================================================================
def iniciarFiltroLaplacebyOrder ( imagen, kernel, ordenGaussian ):
    global gray
    if ordenGaussian == 0:
        gray = ReadWrittenIMGs.readIMGBORDER( imagen )
    if ordenGaussian == 1 or ordenGaussian == 2:
        gray = ReadWrittenIMGs.readIMGBORDER( imagen )
        # remove noise
        nameImg_gaussian_filter = getPathBynaryByName( 'GAUSSIAN_L' + str( ordenGaussian ) )
        gray = gaussian_filter( gray, sigma=3, order=ordenGaussian)
        ReadWrittenIMGs.writteImgBlack( nameImg_gaussian_filter, gray )# se guarda imagen
        
    '''-----------------------  LAPLACE   ----------------------'''
    nameImg_LAPLACE = getPathBynaryByName( 'LAPLACE' )
    lap = cv2.Laplacian( gray, cv2.CV_64F, ksize = kernel)
    lapnew = np.uint8(np.absolute(lap))
    ReadWrittenIMGs.writteImgBlack( nameImg_LAPLACE, lapnew )# se guarda imagen
    
    return nameImg_gaussian_filter, nameImg_LAPLACE

# =============================================================================
#                        FILTRO CANNY
# =============================================================================
def iniciarFiltroCanny ( imagen, kerneL, threshold_1, threshold_2 ):
    gray = ReadWrittenIMGs.readIMGBORDER( imagen )
    
    nameImg_GaussianBlur = getPathBynaryByName( 'GAUSSIAN_KZISE_C' + str( kerneL ) )
    Img_GaussianBlur = cv2.GaussianBlur(src = gray, ksize = (kerneL,kerneL), sigmaX = 3)
    ReadWrittenIMGs.writteImgBlack( nameImg_GaussianBlur, Img_GaussianBlur )# se guarda imagen
    
    '''-----------------------  CANNY   ----------------------'''
    nameImg_CANNY = getPathBynaryByName( 'CANNY' )
    CANNY = cv2.Canny(Img_GaussianBlur, threshold1 = threshold_1, threshold2 = threshold_2 )
    ReadWrittenIMGs.writteImgBlack( nameImg_CANNY, CANNY )# se guarda imagen
    
    return nameImg_GaussianBlur, nameImg_CANNY
