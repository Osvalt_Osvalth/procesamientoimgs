
import matplotlib.pyplot as plt
from NDVI_files import rutas_D_Archivos as myPath

def getGraphPie(verde, amarillo, rojo, negro):
    pathImg = myPath.getPathByCategory('GRAPH_PIE', histo=None)
    tipos = [verde, amarillo, rojo, negro]
    nombres = ["Verde", "Amarillo", "Rojo", "Negro"]
    colores = ["#00FF00", "#FFFF00", "#FF0000", "#0023A0"]

    plt.pie(tipos, labels=nombres, autopct="%0.1f %%", colors=colores)
    plt.axis("equal")
    plt.savefig(pathImg, dpi=256)  # 2600
    plt.show()
    plt.close()

    return pathImg
