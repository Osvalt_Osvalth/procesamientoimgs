# -*- coding: utf-8 -*-
"""
Created on Fri Jun 26 20:38:49 2020

@author: music
"""
import cv2
from NDVI_files import leerIMGs_TIF as ReadWrittenIMGs
from NDVI_files import ndvi as ReadIMGs
from NDVI_files import rutas_D_Archivos as myPath
from HISTOGRAMA import histograma2 as hist

def getPathBynaryByName(nameImage , histo):
    guardarRutaCategoria = myPath.getPathByCategory(nameImage, histo)
    return guardarRutaCategoria

def getContrastBrightness ( rutaIma, alpha1, beta1 ):
    alpha1_x = (alpha1/10)
    img = ReadIMGs.obtenerImgNIR( rutaIma )
    while (1):
        nameImg, nameHisto = getPathBynaryByName('CONTRASTE_BRILLO', histo = 'HISTOGRAMA')
        adjusted = cv2.convertScaleAbs(img, alpha = alpha1_x, beta = beta1)
        cv2.imwrite( nameImg, adjusted )
        ReadWrittenIMGs.writteImgBlack( nameImg, adjusted )
        nameHisto2 = hist.getHistogram ( nameImg, nameHisto )
        return nameImg, nameHisto2




    