# -*- coding: utf-8 -*-
"""
Created on Sun Jun 28 01:07:52 2020

@author: music
"""
from NDVI_files import leerIMGs_TIF as ReadWrittenIMGs
from NDVI_files import ndvi as ReadIMGs
from NDVI_files import rutas_D_Archivos as myPath
from HISTOGRAMA import histograma2 as hist1

# https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_thresholding/py_thresholding.html

def get_ecualizarHistograma ( imagen ):
    img = ReadIMGs.obtenerImgNIR( imagen )
    #while(1):
    nameImg, nameHisto = myPath.getPathByCategory('ECUALIZACION', histo = 'HISTOGRAMA')
    #img = cv2.equalizeHist( img )
    img = ReadWrittenIMGs.get_Histograma( img )
    ReadWrittenIMGs.writteImgBlack( nameImg, img )
    nameHisto2 = hist1.getHistogram ( nameImg, nameHisto )

    return nameImg, nameHisto2

