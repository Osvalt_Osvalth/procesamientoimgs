import os
from flask import Flask, jsonify, request, flash, redirect, url_for
from flask_cors import CORS
from werkzeug.utils import secure_filename
UPLOAD_FOLDER_PATH = 'RESULTADOS/UPLOADS/'
UPLOAD_FOLDER = './'+UPLOAD_FOLDER_PATH;
ALLOWED_EXTENSIONS = set(['tiff', 'tif', 'TIFF', 'TIF'])
SECRET_KEY = 'POTRERO_UPLOAD_IMAGE_FLASK'

app = Flask(__name__, static_folder='RESULTADOS')

cors = CORS(app)

from NDVI_files import ndvi as ndvi_1
from ecualizacion import ecualizar as ecualizar_1 
from filtros import deteccion_D_Bordes as DetecBordes_1
from realce_D_Bordes import realceBordes as realceBordes_1
from median_Blur import median_Blur as median_Blur_1
#from puntosD_interes import clustering as clustering_1
from puntosD_interes import clustering2 as clustering_1
from ReducirRuido import FiltrarRuido as filtersBLUR_1
from OPERACIONES import operaciones as operations
from OPERACIONES import flipImage as flipImage
from HISTOGRAMA import graphPie as pieGraph
#Modulo de prueba JSON
from api_resultados import api_resultados1

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
#Revisa el tipo de archivos que llegan, y si son permitidos
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
#Crea los folder si no existen
def create_new_folder(local_dir):
    newpath = local_dir
    if not os.path.exists(newpath):
        os.makedirs(newpath)
    return newpath

@app.route('/uploadIMAGES', methods=['GET', 'POST'])
def upload_file():
    print('Entre')
    if request.method == 'POST':
        # checa si existe el archivo de la imagen o si ha llegado parte del mismo
        if SECRET_KEY not in request.files:
            flash('No existe archivo')
            return redirect(request.url)
        file = request.files[SECRET_KEY]
        # Si el usuario no selecciona un archivo, tambien si no ingresa
        # o es valio el formData manda este mensaje
        if file.filename == '':
            flash('El usuario no selecciono un archivo')
            return redirect(request.url)
        #Si la imagen no se ha cargado se realiza lo siguiente
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)#Obtiene el nombre
            create_new_folder(app.config['UPLOAD_FOLDER'])#Configura la ruta donde se va almacenar la imagen por si no existe
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))#Guarda la imagen en la ruta UPLOAD_FOLDER
            #Retorna el dos rutas, la primera es para voyver a hacer una peticion con resto a un procesamiento
            #la otra ruta es para mostrarla en el navegador de la página web
            return jsonify({"PathFolderImageFlask": UPLOAD_FOLDER + filename, "PathURLImageFlask": UPLOAD_FOLDER_PATH + filename})

@app.route('/', methods=['GET'])
def hi():
    return ('Sistema de procesamiento de imágenes', 200)

@app.route('/greet', methods=['GET'])
def greet():
    return ('Sistema de procesamiento de imágenes del Ingenio El Potrero', 200)

@app.route('/operation/flip', methods=['POST'])
def get_flip():
    ImagenA = request.json['ImagenA']
    valueFlip = request.json['valueFlip']
    ImagenRest = flipImage.getFlipImage(ImagenA,valueFlip)
    Objeto = { "ImagenRest": ImagenRest }
    return jsonify({"ROTAR": Objeto })


@app.route('/operation/add', methods=['POST'])
def get_add():
    ImagenA = request.json['ImagenA']
    ImagenB = request.json['ImagenB']
    ImagenRest = operations.getAdd(ImagenA, ImagenB)
    ImagenC = {
        "ImagenRest": ImagenRest,
    }
    return jsonify({"ADD": ImagenC})

@app.route('/operation/subtract', methods=['POST'])
def get_subtract():
    ImagenA = request.json['ImagenA']
    ImagenB = request.json['ImagenB']
    ImagenRest = operations.getAdd(ImagenA, ImagenB)
    ImagenC = {
        "ImagenRest": ImagenRest,
    }
    return jsonify({"SUBTRACT": ImagenC})

@app.route('/ndvi', methods=['POST'])
def get_ndvi():
    print('Estas en la petición NDVI')
    img_NIR_path = request.json['path_NIR']
    img_RED_path = request.json['path_RED']
    img_NDVIbar, img_NDVIsingle, img_NDVI_MAP = ndvi_1.mostrarImgDelNDVI(img_NIR_path, img_RED_path)
    NDVI = {
        "pathNDVI0": img_NDVIsingle,
        "pathNDVI1": img_NDVIbar,
        "pathNDVI2": img_NDVI_MAP,
    }
    return jsonify({"message" : "Image NDVI created Succesfully", "NDVI" : NDVI})

@app.route('/ecualizar', methods=['POST'])
def get_Ecualizar():
    img_path = request.json['path_IMG_Ecualizar']
    path_ECUALIZAR, path_HISTOGRAMA  = ecualizar_1.get_ecualizarHistograma(img_path)
    new_image_ECUALIZAR = { 
        "path_ECUALIZAR" : path_ECUALIZAR, 
        "path_HISTOGRAMA" : path_HISTOGRAMA 
    }    
    # api_resultados1[0]["ECUALIZAR"].append(new_image_ECUALIZAR)
    return jsonify({"message" : "Image ECUALIZATION created Succesfully", "ECUALIZAR" : new_image_ECUALIZAR})

@app.route('/deteccionBordes/sobel', methods=['POST'])
def get_DeteccionBordes_Sobel():
    img_path = request.json['path_IMG']
    kernel = request.json['kernel']
    ordenGaussian = request.json['ordenGaussian']
    path_gaussian_filter, path_sobelX, path_sobelY, path_MAGNITUD = DetecBordes_1.iniciarFiltroSobelbyOrder(img_path, kernel, ordenGaussian)
    new_imageSOBEL = { 
        "path_sobelX" : path_sobelX,
        "path_sobelY" : path_sobelY,
        "path_MAGNITUD" : path_MAGNITUD,
        "path_gaussian_filter" : path_gaussian_filter,
    }
    # api_resultados1[0]["SOBEL"].append(new_imageSOBEL)
    return jsonify({"message" : "Image SOBEL created Succesfully", "SOBEL" : new_imageSOBEL})

@app.route('/deteccionBordes/laplace', methods=['POST'])
def get_DeteccionBordes_LAPLACE():
    img_path = request.json['path_IMG']
    kernel = request.json['kernel']
    ordenGaussian = request.json['ordenGaussian']
    path_gaussian_filter, path_LAPLACE = DetecBordes_1.iniciarFiltroLaplacebyOrder(img_path,kernel,ordenGaussian)
    new_imageLAPLACE = { 
        "path_LAPLACE" : path_LAPLACE,
        "path_gaussian_filter" : path_gaussian_filter,
    }
    # api_resultados1[0]["LAPLACE"].append(new_imageLAPLACE)
    return jsonify({"message" : "Image LAPLACE created Succesfully", "LAPLACE" : new_imageLAPLACE})

@app.route('/deteccionBordes/canny', methods=['POST'])
def get_DeteccionBordes_CANNY():
    img_path = request.json['path_IMG']
    kernel = request.json['kernel']
    threshold_1 = request.json['threshold_1']
    threshold_2 = request.json['threshold_2']
    path_GaussianBlur, path_CANNY = DetecBordes_1.iniciarFiltroCanny(img_path,kernel,threshold_1,threshold_2)
    new_imageCANNY = { 
        "path_GaussianBlur" : path_GaussianBlur, 
        "path_CANNY" : path_CANNY,
    }
    # api_resultados1[0]["CANNY"].append(new_imageCANNY)
    return jsonify({"message" : "Image CANNY created Succesfully", "CANNY" : new_imageCANNY})

@app.route('/realceDeBordes', methods=['POST'])
def get_RealceDeBordes():
    orig_path = request.json['path_IMG_original']
    filt_path = request.json['path_IMG_filter']
    path_REALCE_DE_BORDES  = realceBordes_1.getEdgeEnhancement( orig_path, filt_path )
    new_REALCE_DE_BORDES = { 
        "path_REALCE_DE_BORDES" : path_REALCE_DE_BORDES
    }
    #api_resultados1[0]["REALCE_DE_BORDES"].append(new_REALCE_DE_BORDES)
    return jsonify({"message" : "Image REALCE DE BORDES created Succesfully", "REALCE_DE_BORDES" : new_REALCE_DE_BORDES})

@app.route('/medianBlur', methods=['POST'])
def get_MedianBlur():
    orig_path = request.json['path_IMG_original']
    valueBlur = request.json['value_Median_Blur']
    path_MEDIAN_BLUR, hist_MEDIAN_BLUR = median_Blur_1.calcularMedianBlur( orig_path, valueBlur )
    new_MEDIAN_BLUR = {
        "path_MEDIAN_BLUR" : path_MEDIAN_BLUR,
        "hist_MEDIAN_BLUR" : hist_MEDIAN_BLUR
    }
    # api_resultados1[0]["MEDIAN_BLUR"].append(new_MEDIAN_BLUR)
    return jsonify({"message" : "Image MEDIANBLUR created Succesfully", "MEDIAN_BLUR" : new_MEDIAN_BLUR})

@app.route('/clustering', methods=['POST'])
def get_Clustering():
    orig_path = request.json['path_IMG_original']
    valueKmeans = request.json['value_Kmeans']
    hist_CLUSTERING  = clustering_1.getclustering( orig_path, valueKmeans )
    NEW_CLUSTERING = { 
        "hist_CLUSTERING" : hist_CLUSTERING
    }
    return jsonify({"message" : "Image CLUSTERING created Succesfully", "CLUSTERING" : NEW_CLUSTERING})

@app.route('/filtrarRuidoimpulsivo/medianBlurNoise', methods=['POST'])
def get_MedianBlurNoise():
    orig_path = request.json['path_IMG_original']
    value = request.json['value']
    path_MEDIAN_BLUR_NOISE  = filtersBLUR_1.filter_MedianBlur( orig_path, value )
    new_MEDIAN_BLUR_NOISE = { 
        "path_MEDIAN_BLUR_NOISE" : path_MEDIAN_BLUR_NOISE
    }
    # api_resultados1[0]["MEDIANBLUR_FILTER_NOISE"].append(new_MEDIAN_BLUR_NOISE)
    return jsonify({"message" : "Image FILTRO MEDIAN BLUR created Succesfully", "MEDIANBLUR_FILTER_NOISE" : new_MEDIAN_BLUR_NOISE})

@app.route('/filtrarRuidoimpulsivo/gaussianBlurNoise', methods=['POST'])
def get_GaussianBlurNoise():
    orig_path = request.json['path_IMG_original']
    value = request.json['value']
    path_GAUSSIAN_BLUR_NOISE  = filtersBLUR_1.filter_GaussianBlur( orig_path, value )
    new_GAUSSIAN_BLUR_NOISE = { 
        "path_GAUSSIAN_BLUR_NOISE" : path_GAUSSIAN_BLUR_NOISE
    }
    # api_resultados1[0]["GAUSSIANBLUR_FILTER_NOISE"].append(new_GAUSSIAN_BLUR_NOISE)
    return jsonify({"message" : "Image FILTRO GAUSSIAN BLUR created Succesfully", "GAUSSIANBLUR_FILTER_NOISE" : new_GAUSSIAN_BLUR_NOISE})

if __name__ == "__main__":
    app.run(debug= True, host="0.0.0.0", port=3010)


