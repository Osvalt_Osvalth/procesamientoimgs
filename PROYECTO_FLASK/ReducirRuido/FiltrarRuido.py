
from NDVI_files import leerIMGs_TIF as ReadWrittenIMGs
from NDVI_files import rutas_D_Archivos as myPath

def filter_MedianBlur( pathIMG, val ):
    nameImg = myPath.getPathByCategory( 'MEDIANBLUR_FILTER_NOISE', histo=None )
    imgBlur = ReadWrittenIMGs.get_FilterMedianBlur( pathIMG, val )
    ReadWrittenIMGs.writteImgBlack( nameImg, imgBlur )
    return nameImg

def filter_GaussianBlur( pathIMG, val ):
    nameImg = myPath.getPathByCategory( 'GAUSSIANBLUR_FILTER_NOISE', histo=None )
    imgBlur = ReadWrittenIMGs.get_FilterGaussianBlur( pathIMG, val )
    ReadWrittenIMGs.writteImgBlack( nameImg, imgBlur )
    return nameImg