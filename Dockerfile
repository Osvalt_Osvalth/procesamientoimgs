FROM tensorflow/tensorflow:nightly-py3
#https://stackoverflow.com/questions/12435209/what-does-the-u-option-stand-for-in-pip-instal
RUN pip install -U pip
RUN pip install -U setuptools
RUN pip install Flask
RUN pip install -U flask-cors
RUN pip install opencv-python numpy
RUN pip install -U scikit-learn
RUN pip install -U scipy
RUN pip install -U matplotlib
RUN pip install -U Werkzeug
RUN apt-get update
RUN apt-get install -y libgl1-mesa-dev

RUN mkdir /usr/app

WORKDIR /usr/app