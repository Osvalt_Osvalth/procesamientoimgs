procesamientoIMGs


#install virtualenv
$pip install virtualenv

#create new virtualenv
$virtualenv flask_env_procesamiento

#Install Flask
$pip install Flask

#INSTALL OPENCV
$pip install opencv-python

#installation issues for numpy, scipy and matplotlib and upgrading pip and setuptools within the enviroment fixed everything
$ pip install -U pip
$ pip install -U setuptools

#Install matplotlib
$pip install matplotlib

##Install tkinter
$pip install python-tk ##This

#install sqlite3
pip install pysqlite3

##Repositorio para imagenes de docker
docker hub  https://hub.docker.com/